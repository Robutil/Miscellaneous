cmake_minimum_required(VERSION 3.9)
project(c C CXX)

set(CMAKE_CXX_STANDARD 11)

add_executable(c
        src/main.c
        src/buffers/circular_buffer.c src/buffers/circular_buffer.h
        src/buffers/linear_buffer.c src/buffers/linear_buffer.h
        src/buffers/linked_list.c src/buffers/linked_list.h
        src/buffers/binary_tree.c src/buffers/binary_tree.h
        src/net/protocol.c src/net/protocol.h
        src/net/server.c src/net/server.h)

add_executable(c_unit_test
        tst/runner.cpp
        tst/UnitTests.h
        tst/BinaryTreeTest.cpp tst/BinaryTreeTest.h
        src/buffers/binary_tree.c src/buffers/binary_tree.h
        src/buffers/circular_buffer.c src/buffers/circular_buffer.h
        tst/CppUnitWrapper.cpp tst/CppUnitWrapper.h tst/CircularBufferTest.cpp tst/CircularBufferTest.h)
target_link_libraries(c_unit_test cppunit)