#ifndef C_CIRCULAR_BUFFER_H
#define C_CIRCULAR_BUFFER_H

#include <glob.h> //size_t

/**
 * A CircularBuffer object is a FIFO queue where the end of the queue is connected
 * to the start.
 * */
struct CircularBuffer {
    void *data;
    size_t number_of_elements;
    size_t element_size;

    size_t head_pointer;
    size_t tail_pointer;
    int is_empty;
};

/**
 * Creates a struct CircularBuffer* with a given size
 *
 * @param number_of_elements Number of elements in the buffer
 * @param element_size Size of element in bytes
 *
 * @return Pointer to CircularBuffer or NULL in case of failure
 * */
struct CircularBuffer *circular_buffer_create(size_t number_of_elements, size_t element_size);

/**
 * Destroys a given struct CircularBuffer*. This function frees the memory
 * but does not set the pointer to NULL.
 *
 *@param buffer The buffer to destroy
 * */
void circular_buffer_destroy(struct CircularBuffer *buffer);

/**
 * Pushes a given data element to the provided buffer. If the buffer is full
 * this action shall fail. Data must be of the same length as element_size.
 *
 * @param buffer Buffer to append to
 * @param data Data element which to append, must be of same size as buffer->element_size
 *
 * @return 0 in case of success or -1 if the buffer was full
 * */
int circular_buffer_push(struct CircularBuffer *buffer, void *data);

/**
 * Retrieves the element pointed to by the tail. The circular buffer uses a FIFO structure,
 * which means that this function will return the element which has been in this buffer the
 * longest.
 *
 * @param buffer Buffer which to retrieve data from
 *
 * @return Pointer to element of size buffer->element_size or NULL if the buffer is empty
 * */
void *circular_buffer_peek(struct CircularBuffer *buffer);

/**
 * Implements the same behaviour as circular_buffer_peek but in addition it removes the
 * peeked element from the buffer.
 *
 * @param buffer Buffer which to retrieve and remove data from
 *
 * @return Pointer to element of size buffer->element_size or NULL if the buffer is empty
 * */
void *circular_buffer_pop(struct CircularBuffer *buffer);

/**
 * Calculates how many elements are stored in the given buffer.
 *
 * @param buffer Buffer to look into
 *
 * @return Amount of elements currently in the buffer
 * */
size_t circular_buffer_size(struct CircularBuffer *buffer);

/**
 * Calculates how many elements there are available in the given buffer
 *
 * @param buffer Buffer to check
 *
 * @return Maximum amount of elements that can be written
 * */
size_t circular_buffer_available(struct CircularBuffer *buffer);

/**
 * Empties and fully resets the given buffer, as it it were newly created. It keeps the
 * element_size and number_of_elements from when it was first created.
 *
 * @param buffer Buffer to reset
 * */
void circular_buffer_reset(struct CircularBuffer *buffer);

/**
 * Prints a human readable representation of the provided buffer. The data itself
 * is not printed.
 *
 * @param buffer Buffer to print
 * */
void circular_buffer_log(struct CircularBuffer *buffer);

#endif //C_CIRCULAR_BUFFER_H
