#ifndef C_BINARY_TREE_H
#define C_BINARY_TREE_H

#include <glob.h> //size_t

/**
 * A Binary Tree is a linked list of nodes where each node has an id and two
 * children. The first set of children, the lower bound, point to a linked list
 * of nodes with smaller id's. The second set of children is the upper bound,
 * which in turn points to a list of nodes with larger id's. This makes it
 * possible to perform more efficient searches than in a linked list. In addition
 * to the id and children pointers this container also has a data pointer and a
 * link to its parent.
 * */
struct BinaryTree {
    int id;
    void *data;
    size_t data_length;

    struct BinaryTree *parent;
    struct BinaryTree *lower;
    struct BinaryTree *upper;
};

/**
 * Creates a BinaryTree node with a specific id. This node does not have
 * any data or relations with other nodes. This node can be cleaned up by
 * calling the remove or destroy method.
 *
 * @param id Id node to create
 *
 * @return A pointer to a newly allocated BinaryTree node
 * */
struct BinaryTree *binary_tree_create(int id);

/**
 * Destroys the given node and all children nodes, all data and containers
 * are freed.
 *
 * @param root Node to destroy, along with its children
 * */
void binary_tree_destroy(struct BinaryTree *root);

/**
 * Destroys the given link and keeps the rest of the tree intact. If the
 * given node has children and/or a parent these will be inherited. If the
 * root node is provided as the link to remove the root node will continue
 * to exist in the same address, but it will contain a different node. If
 * the provided id does not exist in the tree this operation will do nothing.
 *
 * @param root Root node of the tree, the root node and all its children
 * are candidates that can be removed
 * @param id Id of the node to remove
 * */
void binary_tree_remove(struct BinaryTree *root, int id);

/**
 * Attach data to a given node. This operation involves a memcpy.
 *
 * @param node Node to attach data to
 * @param data Pointer to data to add, data will be copied
 * @param data_length Amount of bytes to copy from data
 *
 * @return 0 if the data was added successfully, -1 on error
 * */
int binary_tree_add_data(struct BinaryTree *node, void *data, size_t data_length);

/**
 * Removes data attached to the given node. If no data was attached
 * this function will have no effect. Other than removing the data
 * the node will not be harmed.
 *
 * @param node Node to remove data from.
 * */
void binary_tree_remove_data(struct BinaryTree *node);

/**
 * Inserts a node into the given tree.
 *
 * @param root Root node of the tree
 * @param new_link Node to insert in the tree
 *
 * @return 0 if the insertions went successful, -1 otherwise
 * */
int binary_tree_insert(struct BinaryTree *root, struct BinaryTree *new_link);

/**
 * An ease of use function that combines the creation of a node, adding data
 * and inserting it into the tree. If data is NULL or data_length is zero a
 * node will still be added, just without data.
 *
 * @param root Root node of the tree
 * @param id Id of the node to create
 * @param data Pointer to data to be stored in the node, this data will be copied
 * @param data_length Amount of bytes to be copied from data
 *
 * @return 0 if the node was added successfully, -1 on error
 * */
int binary_tree_create_and_insert(struct BinaryTree *root, int id, void *data, size_t data_length);

/**
 * Retrieves the node with the given id from a tree.
 *
 * @param root The root node of the tree
 * @param id Id of the node to retrieve
 *
 * @return Node with the given id or NULL if the node was not found
 * */
struct BinaryTree *binary_tree_get(struct BinaryTree *root, int id);

/**
 * Finds the root element of the tree; a node which has no parent.
 *
 * @param node Any node of a tree
 *
 * @return The root node of the tree or NULL if the provided node was NULL
 * */
struct BinaryTree *binary_tree_find_root(struct BinaryTree *node);

/**
 * Finds the element with the smallest id in the given tree.
 *
 * @param root Root node of the tree, will search root and all children
 *
 * @return Pointer to the node with the smallest id, or NUll if the provided root was NULL
 * */
struct BinaryTree *binary_tree_find_minimum(struct BinaryTree *root);

/**
 * Finds the element with the largest id in the given tree.
 *
 * @param root Root node of the tree, will search root and all children
 *
 * @return Pointer to the node with the largest id, or NUll if the provided root was NULL
 * */
struct BinaryTree *binary_tree_find_maximum(struct BinaryTree *root);

/**
 * Provides a global overview of every node in the tree.
 *
 * @param root Root node to log, and all its children
 * */
void binary_tree_log(struct BinaryTree *root);

/**
 * Logs a given node's id, and its direct connections.
 *
 * @param node Node to log
 * */
void binary_tree_log_node(struct BinaryTree *node);

#endif //C_BINARY_TREE_H
