#ifndef C_SERVER_H
#define C_SERVER_H

#include <glob.h>
#include <stdbool.h>
#include "../buffers/linear_buffer.h"
#include "../buffers/binary_tree.h"

struct ServerData {
    int max_pending_clients;
    int socket;
    bool active;

    struct BinaryTree *clients;
};

struct ClientData {
    int socket;
    struct LinearBuffer *buffer;
};

#endif //C_SERVER_H
