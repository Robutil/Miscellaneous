//#include <netdb.h>
//#include <string.h>
//#include <stdlib.h>
//#include <stdio.h>
//
//#include "server.h"
//
//#define SOCKET_ERROR (-1)
//#define DEFAULT_MAX_PENDING_CLIENTS 1024
//
//struct _ServerContainer {
//    struct ServerData *server_data;
//    fd_set *fd_read;
//    fd_set *fd_active;
//
//    void (*handler)(struct ClientData *client_data);
//};
//
//void
//_server_check_fds(fd_set *fd_read, struct ServerData *server_data, void (*handler)(struct ClientData *client_data)) {
//    for (int i = 0; i < FD_SETSIZE; ++i) {
//        if (FD_ISSET(i, fd_read)) {
//            if (i == server_data->socket) {
//
//            } else {
//
//            }
//        }
//    }
//}
//
//void _server_add_new_client()
//
//        struct ServerData *server_create(int port) {
//    if (port <= 0) {
//        fprintf(stderr, "[Server](create): Cannot use port %d\n", port);
//        return NULL;
//    }
//
//    struct ServerData *server_data = malloc(sizeof(struct ServerData));
//    if (!server_data) {
//        perror("[Server](create): Cannot allocate memory for container");
//        return NULL;
//    }
//
//    //Get info
//    struct addrinfo hints, *server_info = NULL;
//    memset(&hints, 0, sizeof(struct addrinfo));
//    hints.ai_family = AF_INET;
//    hints.ai_socktype = SOCK_STREAM;
//    hints.ai_flags = AI_PASSIVE;
//
//    char port_buffer[10];
//    sprintf(port_buffer, "%d", port);
//    if (getaddrinfo(NULL, port_buffer, &hints, &server_info) != 0) {
//        perror("[Server](create): Address info failed.");
//        goto abort;
//    }
//
//    //Create socket
//    server_data->socket = socket(hints.ai_family, hints.ai_socktype, hints.ai_flags);
//    if (server_data->socket == SOCKET_ERROR) {
//        perror("[Server](create): Cannot create socket");
//        goto abort;
//    }
//
//    //Make socket reusable
//    int enable = 1;
//    if (setsockopt(server_data->socket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) == SOCKET_ERROR) {
//        perror("[Server](create): Cannot set socket to reusable");
//        goto abort;
//    }
//
//    //Bind
//    if (bind(server_data->socket, server_info->ai_addr, server_info->ai_addrlen) == SOCKET_ERROR) {
//        perror("[Server](create): Cannot bind");
//        goto abort;
//    }
//
//    //Success
//    server_data->max_pending_clients = DEFAULT_MAX_PENDING_CLIENTS;
//    server_data->active = false;
//    server_data->clients = NULL;
//    return server_data;
//
//    abort:
//    free(server_data);
//    free(server_info);
//    return NULL;
//}
//
//void server_serve(struct ServerData *server_data, void (*handler)(struct ClientData *client_data)) {
//    if (!server_data || !handler) {
//        fprintf(stderr, "[Server](serve): Cannot serve null\n");
//        return;
//    }
//
//    //Start listening for incoming connections
//    if (listen(server_data->socket, server_data->max_pending_clients) == SOCKET_ERROR) {
//        perror("[Server](serve): Cannot listen");
//        return;
//    }
//
//    //Set synchronous I/O
//    fd_set fd_active, fd_read;
//    FD_ZERO(&fd_active);
//    FD_SET(server_data->socket, &fd_active);
//    server_data->active = true;
//
//    //Dump variables in container to ease function parameters size
//    struct _ServerContainer server_container = {
//            server_data,
//            &fd_read,
//            &fd_active,
//
//    };
//
//    while (server_data->active) {
//        fd_read = fd_active;
//
//        if (pselect(FD_SETSIZE, &fd_read, NULL, NULL, NULL, NULL) == SOCKET_ERROR) {
//            perror("[Server](serve): Select failed");
//            server_data->active = false;
//            return;
//        }
//
//        _server_check_fds(&fd_read, server_data, handler);
//    }
//}