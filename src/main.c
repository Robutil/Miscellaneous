#include <stdio.h>
#include <stdlib.h>

#include "buffers/circular_buffer.h"
#include "buffers/linear_buffer.h"
#include "buffers/linked_list.h"
#include "buffers/binary_tree.h"


struct CustomDataExample {
    char dat;
    char tad;
};

void print_custom_data(struct CustomDataExample *data) {
    printf("Data: %c %c\n", data->dat, data->tad);
}

void test_circular_buffer() {
    //Forge some testing data that has an element length > 1
    struct CustomDataExample data[10];
    for (int i = 0; i < 10; ++i) {
        data[i].dat = (char) ('0' + i);
        data[i].tad = (char) ('9' - i);
    }

    printf("Sizeof: %d\n", (int) sizeof(struct CustomDataExample)); //2

    //Create buffer for our element type, store three max
    struct CircularBuffer *buffer = circular_buffer_create(3, sizeof(struct CustomDataExample));
    circular_buffer_log(buffer); //It's empty

    //Fill everything
    for (int j = 0; j < 3; ++j) {
        printf("Pushed: %c %c\n", data[j].dat, data[j].tad);
        circular_buffer_push(buffer, &data[j]);
    }
    circular_buffer_log(buffer);

    //Peek
    struct CustomDataExample *temp = circular_buffer_peek(buffer);
    printf("Peeked: %c %c\n", temp->dat, temp->tad);

    //Get 2 elements
    for (int k = 0; k < 2; ++k) {
        struct CustomDataExample *p = circular_buffer_pop(buffer);
        printf("Popped: %c %c\n", p->dat, p->tad);
    }
    circular_buffer_log(buffer);

    //Add a 1 more data
    printf("Pushed %c %c\n", data[4].dat, data[4].tad);
    circular_buffer_push(buffer, &data[4]);

    //Get 2 elements, again
    for (int k = 0; k < 2; ++k) {
        struct CustomDataExample *p = circular_buffer_pop(buffer);
        printf("Popped: %c %c\n", p->dat, p->tad);
    }

    circular_buffer_log(buffer);
    circular_buffer_destroy(buffer);
}

void test_linear_buffer() {
    //Data to store
    char my_bytes[] = "0123456789";

    //Allocate only 6 byte in buffer
    struct LinearBuffer *buffer = linear_buffer_create(6);

    //Completely fill buffer
    printf("Pushed: 012345\n");
    linear_buffer_push(buffer, my_bytes, 6);
    linear_buffer_log(buffer);

    //Read 4 bytes, so 2 left in buffer at the very end
    char *data = linear_buffer_pop_bytes(buffer, 4);
    printf("Popped: %.*s Peek: %.*s\n", 4, data, (int) linear_buffer_size(buffer), (char *) linear_buffer_peek(buffer));
    linear_buffer_log(buffer);
    free(data);


    //Write 4 bytes, buffer should memmove to make sure everything fits
    linear_buffer_push(buffer, &my_bytes[6], 4);
    printf("Pushed: 6789\n");

    //Read all data
    size_t len = linear_buffer_size(buffer);
    data = linear_buffer_pop(buffer);
    printf("Popped: %.*s\n", (int) len, data);
    free(data);

    //Buffer should automatically reset, from tail=6, head=6 to tail=0, head=0
    linear_buffer_log(buffer);

    //Clean up
    linear_buffer_destroy(buffer);
}

void test_linked_list() {
    //Forge some testing data that has an element length > 1
    struct CustomDataExample data[10];
    for (int i = 0; i < 10; ++i) {
        data[i].dat = (char) ('0' + i);
        data[i].tad = (char) ('9' - i);
    }

    //Create root
    struct LinkedList *list = linked_list_create(&data[0], sizeof(struct CustomDataExample));

    //Add some data
    for (int j = 4; j < 10; ++j) {
        linked_list_push(&data[j], sizeof(struct CustomDataExample), list);
    }

    //Show what it looks like, make it circular for fun
    linked_list_log_full(list);
    linked_list_make_circular(list);
    linked_list_log_full(list);

    //Remove 3rd node
    struct LinkedList *link_to_remove = list;
    for (int k = 0; k < 3; ++k) {
        link_to_remove = link_to_remove->child;
    }
    linked_list_remove(link_to_remove);

    //Show what it looks like
    linked_list_log_full(list);

    //Cleanup
    linked_list_destroy(list);
}

void test_binary_tree() {
    binary_tree_insert(NULL, NULL);

    struct CustomDataExample data[10];
    for (int i = 0; i < 10; ++i) {
        data[i].dat = (char) ('0' + i);
        data[i].tad = (char) ('9' - i);
    }


    struct BinaryTree *root = binary_tree_create(6);
    binary_tree_add_data(root, &data[6], sizeof(struct CustomDataExample));

    binary_tree_create_and_insert(root, 4, &data[4], sizeof(struct CustomDataExample));
    binary_tree_create_and_insert(root, 8, &data[8], sizeof(struct CustomDataExample));
    binary_tree_create_and_insert(root, 3, &data[3], sizeof(struct CustomDataExample));
    binary_tree_create_and_insert(root, 7, &data[7], sizeof(struct CustomDataExample));
    binary_tree_create_and_insert(root, 2, &data[2], sizeof(struct CustomDataExample));
    binary_tree_create_and_insert(root, 5, &data[5], sizeof(struct CustomDataExample));
    binary_tree_create_and_insert(root, 9, &data[9], sizeof(struct CustomDataExample));

    struct BinaryTree *node = binary_tree_get(root, 2);
    print_custom_data(node->data);
    print_custom_data(&data[2]);

    binary_tree_log(root);
    binary_tree_remove(root, 6);
    printf("----------\n");
    binary_tree_log(root);

    binary_tree_log_node(binary_tree_get(root, 7));
    binary_tree_log_node(binary_tree_get(root, 8));
    binary_tree_log_node(binary_tree_get(root, 3));

    binary_tree_remove(root, 2);
    printf("----------\n");
    binary_tree_log(root);

    binary_tree_log_node(binary_tree_get(root, 3));
    binary_tree_log_node(binary_tree_get(root, 7));
    binary_tree_log_node(binary_tree_get(root, 9));

    binary_tree_remove(root, 8);
    printf("----------\n");
    binary_tree_log(root);

    binary_tree_log_node(binary_tree_get(root, 7));
    binary_tree_log_node(binary_tree_get(root, 9));

    binary_tree_create_and_insert(root, 6, &data[6], sizeof(struct CustomDataExample));
    printf("----------\n");
    binary_tree_log(root);

    binary_tree_destroy(root);
}

int main() {
    printf("=== Testing Circular Buffer ===\n\n");
    test_circular_buffer();

    printf("\n=== Testing Linear Buffer ===\n\n");
    test_linear_buffer();

    printf("\n=== Testing Linked List ===\n\n");
    test_linked_list();

    printf("\n=== Testing Binary Tree ===\n\n");
    test_binary_tree();

    return 0;
}