#ifndef BINARY_TREE_TEST_H
#define BINARY_TREE_TEST_H

#include "cppunit/extensions/HelperMacros.h"

extern "C" {
#include "../src/buffers/binary_tree.h"
}

class BinaryTreeTest : public CPPUNIT_NS::TestFixture {
public: //CPP UNIT SPECIFIC
CPPUNIT_TEST_SUITE (BinaryTreeTest);
        CPPUNIT_TEST (attachData);
        CPPUNIT_TEST (removeData);
    CPPUNIT_TEST_SUITE_END ();

public:
    BinaryTreeTest();

    void setUp() override;

    void tearDown() override;

    void attachData();

    void removeData();

private:

    struct BinaryTree *root = NULL; //NOLINT

    void assertEmptyNodeWithId(BinaryTree *node, int id);
};

#endif
