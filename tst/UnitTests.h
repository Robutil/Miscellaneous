#ifndef	__UNITTESTS_H
#define	__UNITTESTS_H

#include <cppunit/extensions/HelperMacros.h>

// Registers the fixture into the 'registry'

#include "BinaryTreeTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION (BinaryTreeTest);

#include "CircularBufferTest.h"
CPPUNIT_TEST_SUITE_REGISTRATION (CircularBufferTest);

#endif	//	__UNITTESTS_H

