#ifndef C_CIRCULARBUFFERTEST_H
#define C_CIRCULARBUFFERTEST_H

#include "cppunit/extensions/HelperMacros.h"

extern "C" {
#include "../src/buffers/circular_buffer.h"
};

class CircularBufferTest : public CPPUNIT_NS::TestFixture {
public: //CPP UNIT SPECIFIC
CPPUNIT_TEST_SUITE (CircularBufferTest);
        CPPUNIT_TEST (something);
    CPPUNIT_TEST_SUITE_END ();

public:
    CircularBufferTest();

    void setUp() override;

    void tearDown() override;

    void something();

private:

    struct CircularBuffer *root = NULL; //NOLINT
};


#endif //C_CIRCULARBUFFERTEST_H
