#include <cstring>

#include "BinaryTreeTest.h"

#define ROOT_ID 6

BinaryTreeTest::BinaryTreeTest() = default;

void BinaryTreeTest::setUp() {
    root = binary_tree_create(ROOT_ID);
    assertEmptyNodeWithId(root, ROOT_ID);
}

void BinaryTreeTest::tearDown() {
    binary_tree_destroy(root);
    root = NULL; //NOLINT
}

void BinaryTreeTest::assertEmptyNodeWithId(struct BinaryTree *node, int id) {
    CPPUNIT_ASSERT_MESSAGE("[BinaryTreeTest](setUp): Failed to init container", node != NULL); //NOLINT
    CPPUNIT_ASSERT(node->id == id);
    CPPUNIT_ASSERT(node->data == NULL); //NOLINT
    CPPUNIT_ASSERT(node->data_length == 0);
    CPPUNIT_ASSERT(node->lower == NULL); //NOLINT
    CPPUNIT_ASSERT(node->upper == NULL); //NOLINT
    CPPUNIT_ASSERT(node->parent == NULL); //NOLINT
}

void BinaryTreeTest::attachData() {
    //Forge some data
    size_t buffer_size = 8;
    char temporary_buffer[buffer_size];
    sprintf(temporary_buffer, "id:%d\n", ROOT_ID);

    //Attach data to root node
    binary_tree_add_data(root, temporary_buffer, buffer_size);

    //Check if it was attached
    CPPUNIT_ASSERT_MESSAGE("[BinaryTreeTest](setUp): Did not attach data correctly", root->data_length == buffer_size);
    CPPUNIT_ASSERT(root->data != NULL); //NOLINT

    auto root_data = static_cast<char *>(root->data);
    CPPUNIT_ASSERT(strcmp(root_data, temporary_buffer) == 0);
}

void BinaryTreeTest::removeData() {
    attachData(); //TODO: make a better attach function

    binary_tree_remove_data(root);
    assertEmptyNodeWithId(root, ROOT_ID);
}


