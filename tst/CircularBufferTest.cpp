#include "CircularBufferTest.h"

#define NUMBER_OF_ELEMENTS 6
#define ELEMENT_SIZE 2

CircularBufferTest::CircularBufferTest() = default;

void CircularBufferTest::setUp() {
    root = circular_buffer_create(NUMBER_OF_ELEMENTS, ELEMENT_SIZE);
}

void CircularBufferTest::tearDown() {
    circular_buffer_destroy(root);
}

void CircularBufferTest::something() {
    CPPUNIT_ASSERT(root != NULL); //NOLINT
}
